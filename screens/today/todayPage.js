import React, {Component, useContext} from 'react';
import {View, StyleSheet, Image, Text} from 'react-native';
import {connect} from 'react-redux';
import MainSection from './component/mainSection';
import Animated, {Easing} from 'react-native-reanimated';
import TochableView from '../commonComponent/touchableView';
import Constant from '../../helper/constant';
import {checkBadgeCount} from '../../helper/appHelper';

class TodayPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      badgeCount: 0,
    };
  }

  onAddFilter = () => {};

  updateData = (data) => {
    this.setState({
      badgeCount: checkBadgeCount(data),
    });
  };

  renderAddSelection = () => {
    const {btnPostAdvise, badgeIcon, badgeText} = styles;

    return (
      <TochableView
        onPress={this.onAddFilter}
        pressInColor={'red'}
        backColor={'red'}
        style={[btnPostAdvise, {backgroundColor: Constant.appBackgroundColor}]}>
        <Image
          resizeMode="contain"
          style={{height: 14, width: 14}}
          source={{uri: 'icon_plus'}}
        />
        {this.state.badgeCount !== 0 && (
          <View style={[styles.badgeIcon]}>
            <Text style={badgeText} numberOfLines={1}>
              {this.state.badgeCount}
            </Text>
          </View>
        )}
      </TochableView>
    );
  };

  render() {
    return (
      <View style={[styles.container]}>
        <Animated.ScrollView
          scrollEventThrottle={16}
          automaticallyAdjustContentInsets={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            top: this.props.safeAreaInsetsDefault.top + 50,
          }}
          style={styles.container}>
          <MainSection updateData={this.updateData} />
        </Animated.ScrollView>
        {this.renderAddSelection()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  btnPostAdvise: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Constant.transparent,
    position: 'absolute',
    bottom: 15,
    right: 24,
    height: 48,
    width: 48,
    borderRadius: 24,
  },
  badgeIcon: {
    overflow: 'visible',
    bottom: 30,
    minWidth: 18,
    maxWidth: 26,
    position: 'absolute',
    backgroundColor: 'red',
    alignItems: 'center',
    height: 18,
    borderRadius: 9,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    left: 0,
    top: -5,
  },
  badgeText: {
    color: '#FFF',
    backgroundColor: 'transparent',
    padding: 2,
    fontFamily: 'System',
    fontWeight: '600',
    fontSize: 12,
  },
});

const mapStateToProps = (state) => {
  return {
    safeAreaInsetsDefault: state.user.safeAreaInsetsDefault,
  };
};

export default connect(mapStateToProps, {})(TodayPage);
