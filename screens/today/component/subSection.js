import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {cloneDeep, find} from 'lodash';
import {OnlyTextSubSection} from '../subComponent/subSection/onlyTextSubSection';
import {SelectionOptionSection} from '../subComponent/subSection/selectionOptionSection';
import {OverUnderSection} from '../subComponent/subSection/overUnderSection';
import {OverUnderMixSection} from '../subComponent/subSection/overUnderMixSection';
import Constant from '../../../helper/constant';

let currentPickerValue = '';

export default class MainSectionComponent extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  onOpenPicker = () => {
    // Set default value when open picker
    currentPickerValue = '';
  };

  //On done button press in picker
  onValueChange = (value, type, title) => {
    currentPickerValue = value;
    if (Constant.isANDROID) {
      this.onFilterUpdate(title, type);
    }
  };

  onFilterUpdate = (title, type) => {
    // If no any option change then no call to update data
    if (currentPickerValue !== '') {
      const {data} = this.props;

      let dataObj = data[title];

      let objDefault = find(dataObj, {value: currentPickerValue});
      let objIndex = 0;

      // If object is undefined then chck value with float value
      if (objDefault === undefined) {
        objDefault = find(dataObj, {value: parseFloat(currentPickerValue)});
        objIndex = dataObj.indexOf(objDefault);
      } else {
        objIndex = dataObj.indexOf(objDefault);
      }

      for (let i = 0; i < data[title].length; i++) {
        if (data[title][i].isChange && data[title][i].isChange.length !== 0) {
          if (data[title][i].isChange.includes(type)) {
            let objIndex = data[title][i].isChange.indexOf(type);
            data[title][i].isChange.splice(objIndex, 1);
          }
        }
      }

      if (
        data[title][objIndex].isChange &&
        data[title][objIndex].isChange.length !== 0
      ) {
        data[title][objIndex].isChange.push(type);
      } else {
        data[title][objIndex].isChange = [type];
      }

      //If change data when unselected right box
      let obj = find(data[title], 'isSelected');
      if (obj) {
        let objIndex = data[title].indexOf(obj);
        if (data[title][objIndex].isSelected.includes(type)) {
          let objIndex = data[title][data[title].length - 1].isSelected.indexOf(
            type,
          );
          data[title][data[title].length - 1].isSelected.splice(objIndex, 1);
        }
      }
      this.props.updateData(data);
    }
  };

  onCheckType = (data, index, keyValue, mainArr) => {
    let objValue = Object.keys(data);
    if (objValue && objValue.length !== 0 && objValue.includes('Away')) {
      return '1';
    } else {
      let obj = find(data, 'Away');
      if (obj) {
        return '2';
      } else {
        let objOver = find(data, 'Over');
        if (objOver) {
          if (keyValue === 'Team Over') {
            return '4';
          } else if (keyValue === 'Away Totals') {
            if (mainArr.includes('Team Over')) {
              return '0';
            } else {
              return '4';
            }
          }
          return '3';
        }
      }
    }
    return '1';
  };

  onEventClick = (type, title, key) => {
    let {data} = this.props;

    switch (type) {
      case 1:
        if (data[title].isSelected && data[title].isSelected.length !== 0) {
          if (data[title].isSelected.includes(key)) {
            let objIndex = data[title].isSelected.indexOf(key);
            data[title].isSelected.splice(objIndex, 1);
          } else {
            // data[title].isSelected.push(key);
            data[title].isSelected = [key];
          }
        } else {
          data[title].isSelected = [key];
        }
        this.props.updateData(data);
        break;
      case 2:
        let obj = find(data[title], 'isSelected');
        if (obj && obj.isSelected.length !== 0) {
          if (data[title][data[title].length - 1].isSelected.includes(key)) {
            let objIndex = data[title][
              data[title].length - 1
            ].isSelected.indexOf(key);
            data[title][data[title].length - 1].isSelected.splice(objIndex, 1);
          } else {
            // data[title][data[title].length - 1].isSelected.push(key);
            data[title][data[title].length - 1] = {isSelected: [key]};
          }
        } else {
          if (obj) {
            data[title][data[title].length - 1] = {isSelected: [key]};
          } else {
            data[title].push({
              isSelected: [key],
            });
          }
        }
        this.props.updateData(data);
        break;
      case 3:
        let obj4 = find(data[title], 'isSelected');
        let checkCurrentGet = false;
        if (obj4 && obj4.isSelected.length !== 0) {
          if (data[title][data[title].length - 1].isSelected.includes(key)) {
            checkCurrentGet = true;
            let objIndex = data[title][
              data[title].length - 1
            ].isSelected.indexOf(key);
            data[title][data[title].length - 1].isSelected.splice(objIndex, 1);
          } else {
            data[title][data[title].length - 1].isSelected.push(key);
          }
        } else {
          if (obj4) {
            data[title][data[title].length - 1] = {isSelected: [key]};
          } else {
            data[title].push({
              isSelected: [key],
            });
          }
        }

        if (checkCurrentGet === false) {
          title = title === 'Team Over' ? 'Away Totals' : 'Team Over';
          let objNew = find(data[title], 'isSelected');
          if (objNew && objNew.isSelected.length !== 0) {
            if (data[title][data[title].length - 1].isSelected.includes(key)) {
              checkCurrentGet = true;
              let objIndex = data[title][
                data[title].length - 1
              ].isSelected.indexOf(key);
              data[title][data[title].length - 1].isSelected.splice(
                objIndex,
                1,
              );
            }
          }
        }

        this.props.updateData(data);
        break;
    }
  };

  render() {
    const {container} = styles;
    const {data} = this.props;
    let arr_Obj = Object.keys(data);

    return (
      <View style={[container]}>
        <Animated.ScrollView
          scrollEventThrottle={16}
          horizontal={true}
          automaticallyAdjustContentInsets={false}
          showsHorizontalScrollIndicator={false}
          style={[container]}>
          {arr_Obj.map((obj, index) => {
            let type = this.onCheckType(
              data[arr_Obj[index]],
              index,
              arr_Obj[index],
              arr_Obj,
            );
            if (type === '1') {
              return (
                <OnlyTextSubSection
                  title={arr_Obj[index]}
                  data={data[arr_Obj[index]]}
                  onEventClick={this.onEventClick}
                />
              );

            } else if (type === '2') {
              return (
                <SelectionOptionSection
                  title={arr_Obj[index]}
                  data={data[arr_Obj[index]]}
                  onEventClick={this.onEventClick}
                  onOpenPicker={this.onOpenPicker}
                  onValueChange={this.onValueChange}
                  onFilterUpdate={this.onFilterUpdate}
                />
              );
            } else if (type === '3') {
              return (
                <OverUnderSection
                  title={arr_Obj[index]}
                  data={data[arr_Obj[index]]}
                  onEventClick={this.onEventClick}
                  onOpenPicker={this.onOpenPicker}
                  onValueChange={this.onValueChange}
                  onFilterUpdate={this.onFilterUpdate}
                />
              );
            } else if (type === '4') {
              return (
                <OverUnderMixSection
                  title={arr_Obj[index]}
                  title2={'Away Totals'}
                  data={data['Team Over'] || []}
                  data2={data['Away Totals'] || []}
                  onEventClick={this.onEventClick}
                  onOpenPicker={this.onOpenPicker}
                  onValueChange={this.onValueChange}
                  onFilterUpdate={this.onFilterUpdate}
                />
              );
            }
            if (type !== '0') {
              return (
                <View
                  style={{
                    width: 20,
                    height: '100%',
                    backgroundColor: 'pink',
                    marginRight: 10,
                  }}
                />
              );
            }
          })}
        </Animated.ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
