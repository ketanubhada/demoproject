import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {cloneDeep} from 'lodash';
import Constant from '../../../helper/constant';
import {initialState} from '../subComponent/listingData';
import {PickerController} from '../../commonComponent/pickerController';
import SubSection from './subSection';
import TochableView from '../../commonComponent/touchableView';

let currentPickerValue = '';

export default class MainSectionComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = cloneDeep(initialState);
  }

  onOpenPicker = () => {
    // Set default value when open picker
    currentPickerValue = '';
  };

  getCategoryPickerReturn = (data) => {
    let objArray = [];
    for (let i = 0; i < data.length; i++) {
      objArray.push({value: data[i], label: data[i]});
    }
    return objArray;
  };

  updateData = (data) => {
    const {dropdown1} = this.state;

    dropdown1[dropdown1.mainList.isSelected].data[
      dropdown1[dropdown1.mainList.isSelected].isSelected
    ] = data;

    this.setState({
      dropdown1: cloneDeep(dropdown1),
    });
    this.props.updateData(dropdown1);
  };

  //On done button press in picker
  onValueChange = (value, type) => {
    currentPickerValue = value;
    if (Constant.isANDROID) {
      this.onFilterUpdate(type);
    }
  };

  //Start filter
  onAddFilter = () => {};

  onFilterUpdate = (type) => {
    const {dropdown1} = this.state;

    // If no any option change then no call to update data
    if (currentPickerValue !== '') {
      switch (type) {
        case 'filter1':
          dropdown1.mainList.isSelected = currentPickerValue;

          this.setState({
            dropdown1: cloneDeep(dropdown1),
          });
          this.props.updateData(dropdown1);
          break;
        case 'filter2':
          dropdown1[
            dropdown1.mainList.isSelected
          ].isSelected = currentPickerValue;

          this.setState({
            dropdown1: cloneDeep(dropdown1),
          });
          this.props.updateData(dropdown1);
          break;
      }
    }
  };

  renderFirstTab = () => {
    const {titleFont, tabBox, imageIcon, pickerView} = styles;
    const {dropdown1} = this.state;

    return (
      <View style={tabBox}>
        <Text style={[titleFont]}>{dropdown1.mainList.isSelected}</Text>
        <Image
          resizeMode="contain"
          style={imageIcon}
          source={{uri: 'bottom_arrow'}}
        />
        <View style={pickerView}>
          <PickerController
            itemsData={this.getCategoryPickerReturn(dropdown1.mainList.data)}
            type={'filter1'}
            onOpenPicker={this.onOpenPicker}
            onValueChange={this.onValueChange}
            onFilterUpdate={this.onFilterUpdate}
          />
        </View>
      </View>
    );
  };

  renderSecondTab = () => {
    const {titleFont, tabBox, imageIcon, pickerView} = styles;
    const {dropdown1} = this.state;

    // let value = Object.keys(dropdown1[dropdown1.mainList.isSelected].data[dropdown1[dropdown1.mainList.isSelected].isSelected]);

    return (
      <TouchableOpacity onPress={() => this.onPressPicker()} style={tabBox}>
        <Text style={[titleFont]}>
          {dropdown1[dropdown1.mainList.isSelected].isSelected}
        </Text>
        <Image
          resizeMode="contain"
          style={imageIcon}
          source={{uri: 'bottom_arrow'}}
        />
        <View style={pickerView}>
          <PickerController
            itemsData={this.getCategoryPickerReturn(
              Object.keys(dropdown1[dropdown1.mainList.isSelected].data),
            )}
            type={'filter2'}
            onValueChange={this.onValueChange}
            onFilterUpdate={this.onFilterUpdate}
          />
        </View>
      </TouchableOpacity>
    );
  };

  renderContainer = () => {
    const {titleFont, teamIcon, lineGapHorizontal} = styles;
    const {dropdown1} = this.state;

    return (
      <View style={{flex: 1, flexDirection: 'row', marginTop: 15}}>
        <View style={{marginRight: 10}}>
          <View style={{flex: 1}} />
          <View
            style={{minHeight: 40, flexDirection: 'row', alignItems: 'center'}}>
            <Image
              resizeMode="contain"
              style={teamIcon}
              source={{uri: 'icon_76ers'}}
            />
            <Text style={[titleFont]}>{'Phi 76ers'}</Text>
          </View>
          <View
            style={{minHeight: 40, flexDirection: 'row', alignItems: 'center'}}>
            <Image
              resizeMode="contain"
              style={teamIcon}
              source={{uri: 'icon_toronto'}}
            />
            <Text style={[titleFont]}>{'Tor Raptors'}</Text>
          </View>
          <View style={lineGapHorizontal} />
        </View>
        <View style={{flex: 1, height: 110}}>
          <SubSection
            data={
              dropdown1[dropdown1.mainList.isSelected].data[
                dropdown1[dropdown1.mainList.isSelected].isSelected
              ]
            }
            updateData={this.updateData}
          />
        </View>
      </View>
    );
  };

  renderAddSelection = () => {
    const {btnPostAdvise} = styles;

    return (
      <TochableView
        onPress={this.onAddFilter}
        pressInColor={'red'}
        backColor={'red'}
        style={[btnPostAdvise, {backgroundColor: Constant.appBackgroundColor}]}>
        <Image
          resizeMode="contain"
          style={{height: 14, width: 14}}
          source={{uri: 'icon_plus'}}
        />
      </TochableView>
    );
  };

  render() {
    const {container, mainView, lineGap} = styles;

    return (
      <View style={[container]}>
        <View style={[mainView, Constant.shadow]}>
          <View style={{width: '100%', height: 30, flexDirection: 'row'}}>
            {this.renderFirstTab()}
            {this.renderSecondTab()}
          </View>
          <View style={lineGap} />
          {this.renderContainer()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 26,
    paddingVertical: 10,
  },
  mainView: {padding: 17, backgroundColor: '#FFF', borderRadius: 19},
  titleFont: {
    fontSize: 16,
    fontFamily: 'System',
    fontWeight: 'bold',
    color: Constant.appTextColorDark,
  },
  tabBox: {
    flex: 0.5,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  imageIcon: {
    height: 15,
    width: 15,
    tintColor: Constant.appTextColorDark,
    marginLeft: 5,
    marginTop: 3,
  },
  teamIcon: {
    height: 25,
    width: 25,
    marginRight: 5,
  },
  pickerView: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
  },
  lineGap: {
    marginHorizontal: 10,
    height: 0.5,
    marginTop: 10,
    backgroundColor: '#f2f2f2',
  },
  lineGapHorizontal: {
    position: 'absolute',
    top: 0,
    right: -10,
    bottom: 0,
    width: 1,
    backgroundColor: '#f2f2f2',
  },
});
