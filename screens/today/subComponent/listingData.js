const dropDown1 = {
  mainList: {
    isSelected: '5Dimes',
    data: ['5Dimes', 'Pinnacle'],
  },
  Pinnacle: {
    isSelected: 'Game',
    data: {
      Game: {
        ML: {
          Away: 4.83,
          Home: 1.21,
        },
        Spread: [
          {
            Away: 1.91,
            Home: 2.0,
            main: true,
            value: 9.5,
          },
          {
            Away: 2.08,
            Home: 1.78,
            main: false,
            value: 8.5,
          },
          {
            Away: 5.08,
            Home: 1.11,
            main: false,
            value: 2.5,
          },
        ],
        'Over/Under': [
          {
            Over: 2.33,
            Under: 1.65,
            cur: true,
            main: true,
            value: 231.5,
          },
          {
            Over: 2.33,
            Under: 1.65,
            cur: false,
            main: false,
            value: 232.5,
          },
          {
            Over: 2.33,
            Under: 1.65,
            cur: false,
            main: false,
            value: 233.5,
          },
        ],
        'Team Over': [
          {
            Over: 5.94,
            Under: 5.91,
            cur: true,
            main: true,
            value: 111.5,
          },
          {
            Over: 5.85,
            Under: 5.01,
            cur: false,
            main: false,
            value: 110.5,
          },
        ],
        'Away Totals': [
          {
            Over: 6.94,
            Under: 1.91,
            cur: true,
            main: true,
            value: 111.5,
          },
          {
            Over: 6.85,
            Under: 2.01,
            cur: false,
            main: false,
            value: 110.5,
          },
        ],
      },
      '1st Half': {
        ML: {
          Away: 4.83,
          Home: 1.21,
        },
        Spread: [
          {
            Away: 1.91,
            Home: 1.91,
            main: true,
            value: 9.5,
          },
          {
            Away: 2.08,
            Home: 1.78,
            main: false,
            value: 8.5,
          },
          {
            Away: 5.08,
            Home: 1.11,
            main: false,
            value: 2.5,
          },
        ],
        'Over/Under': [
          {
            Over: 2.33,
            Under: 1.65,
            cur: true,
            main: true,
            value: 231.5,
          },
          {
            Over: 2.33,
            Under: 1.65,
            cur: false,
            main: false,
            value: 232.5,
          },
          {
            Over: 2.33,
            Under: 1.65,
            cur: false,
            main: false,
            value: 233.5,
          },
        ],
        'Team Over': [
          {
            Over: 5.94,
            Under: 5.91,
            cur: true,
            main: true,
            value: 111.5,
          },
          {
            Over: 5.85,
            Under: 5.01,
            cur: false,
            main: false,
            value: 110.5,
          },
        ],
        'Away Totals': [
          {
            Over: 6.94,
            Under: 1.91,
            cur: true,
            main: true,
            value: 111.5,
          },
          {
            Over: 6.85,
            Under: 2.01,
            cur: false,
            main: false,
            value: 110.5,
          },
        ],
      },
    },
  },
  '5Dimes': {
    isSelected: 'Game2',
    data: {
      Game2: {
        ML: {
          Away: 4.83,
          Home: 1.21,
        },
        Spread: [
          {
            Away: 1.91,
            Home: 2.0,
            main: true,
            value: 9.5,
          },
          {
            Away: 2.08,
            Home: 1.78,
            main: false,
            value: 8.5,
          },
          {
            Away: 5.08,
            Home: 1.11,
            main: false,
            value: 2.5,
          },
        ],
        'Over/Under': [
          {
            Over: 2.33,
            Under: 4.33,
            cur: true,
            main: true,
            value: 231.5,
          },
          {
            Over: 5.23,
            Under: 2.05,
            cur: false,
            main: false,
            value: 232.5,
          },
          {
            Over: 2.83,
            Under: 8.05,
            cur: false,
            main: false,
            value: 233.5,
          },
        ],
        'Team Over': [
          {
            Over: 5.94,
            Under: 4.91,
            cur: true,
            main: true,
            value: 34.5,
          },
          {
            Over: 2.85,
            Under: 1.01,
            cur: false,
            main: false,
            value: 92.5,
          },
        ],
        'Away Totals': [
          {
            Over: 16.94,
            Under: 1.5,
            cur: true,
            main: true,
            value: 23.5,
          },
          {
            Over: 11.94,
            Under: 1.91,
            cur: true,
            main: true,
            value: 221.5,
          },
        ],
      },
      '1st Half2': {
        ML: {
          Away: 2.55,
          Home: 1.21,
        },
        Spread: [
          {
            Away: 1.91,
            Home: 1.91,
            main: true,
            value: 9.5,
          },
          {
            Away: 2.08,
            Home: 1.78,
            main: false,
            value: 8.5,
          },
          {
            Away: 5.08,
            Home: 1.11,
            main: false,
            value: 2.5,
          },
        ],
        'Over/Under': [
          {
            Over: 2.33,
            Under: 1.65,
            cur: true,
            main: true,
            value: 231.5,
          },
          {
            Over: 2.33,
            Under: 1.65,
            cur: false,
            main: false,
            value: 232.5,
          },
          {
            Over: 2.33,
            Under: 1.65,
            cur: false,
            main: false,
            value: 233.5,
          },
        ],
        'Team Over': [
          {
            Over: 5.94,
            Under: 5.91,
            cur: true,
            main: true,
            value: 111.5,
          },
          {
            Over: 5.85,
            Under: 5.01,
            cur: false,
            main: false,
            value: 110.5,
          },
        ],
        'Away Totals': [
          {
            Over: 6.94,
            Under: 1.91,
            cur: true,
            main: true,
            value: 111.5,
          },
          {
            Over: 6.85,
            Under: 2.01,
            cur: false,
            main: false,
            value: 110.5,
          },
        ],
      },
    },
  },
};

const initialState = {
  dropdown1: dropDown1,
};

export {initialState};
