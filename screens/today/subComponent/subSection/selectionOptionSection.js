import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Constant from '../../../../helper/constant';
import {ButtonSelection} from '../../../commonComponent/buttonSelection';
import {find, findIndex} from 'lodash';
import {PickerController} from '../../../commonComponent/pickerController';

const SelectionOptionSection = (props) => {
  const {data, title} = props;
  const {titleFont, buttonText, buttonStyle, mainView, pickerView} = styles;

  let obj = find(data, 'isChange');
  let valueDropDown1 = '';
  let valueDropDown2 = '';
  let valueSelectedValue1 = '';
  let valueSelectedValue2 = '';
  let boolSelected1 = false;
  let boolSelected2 = false;

  //Check already change drop down
  if (obj) {
    for (let i = 0; i < data.length; i++) {
      if (data[i].isChange && data[i].isChange.length !== 0) {
        if (data[i].isChange.includes('Away')) {
          valueDropDown1 = data[i].value;
          valueSelectedValue1 = data[i].Away;
        }
        if (data[i].isChange.includes('Home')) {
          valueDropDown2 = data[i].value;
          valueSelectedValue2 = data[i].Home;
        }
      }
    }
  }

  if (valueDropDown1 === '') {
    let objDefault = find(data, {main: true});
    valueDropDown1 = '-' + objDefault.value;
    valueSelectedValue1 = objDefault.Away;
  }

  if (valueDropDown2 === '') {
    let objDefault = find(data, {main: true});
    valueDropDown2 = objDefault.value;
    valueSelectedValue2 = objDefault.Home;
  }

  //Selected check
  if (data.length !== 0) {
    let obj = find(data, 'isSelected');
    if (obj) {
      if (obj.isSelected) {
        if (obj.isSelected.includes('Away')) {
          boolSelected1 = true;
        }
        if (obj.isSelected.includes('Home')) {
          boolSelected2 = true;
        }
      }
    }
  }

  return (
    <View>
      <View style={{flex: 1}} />
      <Text style={[titleFont]}>{title}</Text>

      <View style={{flexDirection: 'row'}}>
        <View>
          <View style={mainView}>
            <ButtonSelection
              title={valueDropDown1}
              backColor={'white'}
              color={'#000000'}
              shadow={Constant.shadow}
              otherStyle={buttonStyle}
              otherTextStyle={buttonText}
              isArrow={true}
              onPress={() => props.onEventClick(1, title, 'Away')}
            />
            <View style={pickerView}>
              <PickerController
                itemsData={getArrayList(data)}
                type={'filter1'}
                onOpenPicker={props.onOpenPicker}
                onValueChange={(value) =>
                  props.onValueChange(value, 'Away', title)
                }
                onFilterUpdate={() => props.onFilterUpdate(title, 'Away')}
              />
            </View>
          </View>

          <View style={mainView}>
            <ButtonSelection
              title={valueDropDown2}
              backColor={'white'}
              color={'#000000'}
              shadow={Constant.shadow}
              otherStyle={buttonStyle}
              otherTextStyle={buttonText}
              isArrow={true}
              onPress={() => props.onEventClick(1, title, 'Home')}
            />
            <View style={pickerView}>
              <PickerController
                itemsData={getArrayList(data)}
                type={'filter1'}
                onOpenPicker={props.onOpenPicker}
                onValueChange={(value) =>
                  props.onValueChange(value, 'Home', title)
                }
                onFilterUpdate={() => props.onFilterUpdate(title, 'Home')}
              />
            </View>
          </View>
        </View>

        <View>
          <View style={mainView}>
            <ButtonSelection
              title={valueSelectedValue1}
              backColor={(boolSelected1 && Constant.appTextColor) || 'white'}
              color={(boolSelected1 && 'white') || '#000000'}
              shadow={Constant.shadow}
              otherStyle={buttonStyle}
              otherTextStyle={buttonText}
              onPress={() => props.onEventClick(2, title, 'Away')}
            />
          </View>

          <View style={mainView}>
            <ButtonSelection
              title={valueSelectedValue2}
              backColor={(boolSelected2 && Constant.appTextColor) || 'white'}
              color={(boolSelected2 && 'white') || '#000000'}
              shadow={Constant.shadow}
              otherStyle={buttonStyle}
              otherTextStyle={buttonText}
              onPress={() => props.onEventClick(2, title, 'Home')}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const getArrayList = (data) => {
  let objArray = [];
  for (let i = 0; i < data.length; i++) {
    if (data[i].value) {
      objArray.push({value: `${data[i].value}`, label: `${data[i].value}`});
    }
  }
  return objArray;
};

const styles = StyleSheet.create({
  titleFont: {
    fontSize: 16,
    fontFamily: 'System',
    fontWeight: 'bold',
    color: Constant.appTextColorDark,
    textAlign: 'center',
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 15,
    fontFamily: 'System',
    fontWeight: '600',
    paddingHorizontal: 10,
  },
  buttonStyle: {
    height: 30,
    borderRadius: 19,
  },
  mainView: {
    minHeight: 40,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  pickerView: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 100,
  },
});

export {SelectionOptionSection};
