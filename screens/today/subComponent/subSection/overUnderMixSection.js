import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Constant from '../../../../helper/constant';
import {ButtonSelection} from '../../../commonComponent/buttonSelection';
import {find, findIndex} from 'lodash';
import {PickerController} from '../../../commonComponent/pickerController';

const OverUnderMixSection = (props) => {
  const {data, data2, title, title2} = props;
  const {titleFont, buttonText, buttonStyle, mainView, pickerView} = styles;

  let objOver = find(data, 'isChange');
  let objUnder = find(data2, 'isChange');
  let hideOver = data.length === 0 ? true : false;
  let hideUnder = data2.length === 0 ? true : false;

  let valueDropDown1 = '';
  let valueDropDown2 = '';
  let valueDropDown3 = '';
  let valueDropDown4 = '';
  let valueSelectedValue1 = '';
  let valueSelectedValue2 = '';
  let valueSelectedValue3 = '';
  let valueSelectedValue4 = '';
  let boolSelected1 = false;
  let boolSelected2 = false;
  let boolSelected3 = false;
  let boolSelected4 = false;

  //Check already change drop down
  if (objOver) {
    for (let i = 0; i < data.length; i++) {
      if (data[i].isChange && data[i].isChange.length !== 0) {
        if (data[i].isChange.includes('Over')) {
          valueDropDown1 = data[i].value;
          valueSelectedValue1 = data[i].Over;
        }
        if (data[i].isChange.includes('Under')) {
          valueDropDown2 = data[i].value;
          valueSelectedValue2 = data[i].Under;
        }
      }
    }
  }

  if (objUnder) {
    for (let i = 0; i < data2.length; i++) {
      if (data2[i].isChange && data2[i].isChange.length !== 0) {
        if (data2[i].isChange.includes('Over')) {
          valueDropDown3 = data2[i].value;
          valueSelectedValue3 = data2[i].Over;
        }
        if (data2[i].isChange.includes('Under')) {
          valueDropDown4 = data2[i].value;
          valueSelectedValue4 = data2[i].Under;
        }
      }
    }
  }

  if (valueDropDown1 === '' && data.length !== 0) {
    let objDefault = find(data, {main: true});
    valueDropDown1 = objDefault.value;
    valueSelectedValue1 = objDefault.Over;
  }

  if (valueDropDown2 === '' && data.length !== 0) {
    let objDefault = find(data, {main: true});
    valueDropDown2 = objDefault.value;
    valueSelectedValue2 = objDefault.Under;
  }

  if (valueDropDown3 === '' && data2.length !== 0) {
    let objDefault = find(data2, {main: true});
    valueDropDown3 = objDefault.value;
    valueSelectedValue3 = objDefault.Over;
  }

  if (valueDropDown4 === '' && data2.length !== 0) {
    let objDefault = find(data2, {main: true});
    valueDropDown4 = objDefault.value;
    valueSelectedValue4 = objDefault.Under;
  }

  //Selected check
  if (data.length !== 0) {
    let obj = find(data, 'isSelected');
    if (obj) {
      if (obj.isSelected) {
        if (obj.isSelected.includes('Over')) {
          boolSelected1 = true;
        }
        if (obj.isSelected.includes('Under')) {
          boolSelected2 = true;
        }
      }
    }
  }

  if (data2.length !== 0) {
    let obj = find(data2, 'isSelected');
    // alert(JSON.stringify(obj))
    if (obj) {
      if (obj.isSelected) {
        if (obj.isSelected.includes('Over')) {
          boolSelected3 = true;
        }
        if (obj.isSelected.includes('Under')) {
          boolSelected4 = true;
        }
      }
    }
  }

  return (
    <View>
      <View style={{flex: 1}} />
      <View style={{flexDirection: 'row'}}>
        <View>
          <Text style={[titleFont]}>{'Team Over'}</Text>

          <View style={{flexDirection: 'row'}}>
            <View>
              {(!!hideOver === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueDropDown1}
                    backColor={'white'}
                    color={'#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    isArrow={true}
                    onPress={() => props.onEventClick(3, title, 'Over')}
                  />
                  <View style={pickerView}>
                    <PickerController
                      itemsData={getArrayList(data)}
                      type={'filter1'}
                      onOpenPicker={props.onOpenPicker}
                      onValueChange={(value) =>
                        props.onValueChange(value, 'Over', title)
                      }
                      onFilterUpdate={() => props.onFilterUpdate(title, 'Over')}
                    />
                  </View>
                </View>
              )) ||
                emptyData()}

              {(!!hideUnder === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueDropDown3}
                    backColor={'white'}
                    color={'#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    isArrow={true}
                    onPress={() => props.onEventClick(3, title2, 'Over')}
                  />
                  <View style={pickerView}>
                    <PickerController
                      itemsData={getArrayList(data2)}
                      type={'filter1'}
                      onOpenPicker={props.onOpenPicker}
                      onValueChange={(value) =>
                        props.onValueChange(value, 'Over', title2)
                      }
                      onFilterUpdate={() =>
                        props.onFilterUpdate(title2, 'Over')
                      }
                    />
                  </View>
                </View>
              )) ||
                emptyData()}
            </View>

            <View>
              {(!!hideOver === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueSelectedValue1}
                    backColor={
                      (boolSelected1 && Constant.appTextColor) || 'white'
                    }
                    color={(boolSelected1 && 'white') || '#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    onPress={() => props.onEventClick(3, title, 'Over')}
                  />
                </View>
              )) ||
                emptyData()}

              {(!!hideUnder === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueSelectedValue3}
                    backColor={
                      (boolSelected3 && Constant.appTextColor) || 'white'
                    }
                    color={(boolSelected3 && 'white') || '#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    onPress={() => props.onEventClick(3, title2, 'Over')}
                  />
                </View>
              )) ||
                emptyData()}
            </View>
          </View>
        </View>

        <View>
          <Text style={[titleFont]}>{'Team Under'}</Text>

          <View style={{flexDirection: 'row'}}>
            <View>
              {(!!hideOver === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueDropDown2}
                    backColor={'white'}
                    color={'#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    isArrow={true}
                    onPress={() => props.onEventClick(3, title, 'Under')}
                  />
                  <View style={pickerView}>
                    <PickerController
                      itemsData={getArrayList(data)}
                      type={'filter1'}
                      onOpenPicker={props.onOpenPicker}
                      onValueChange={(value) =>
                        props.onValueChange(value, 'Under', title)
                      }
                      onFilterUpdate={() =>
                        props.onFilterUpdate(title, 'Under')
                      }
                    />
                  </View>
                </View>
              )) ||
                emptyData()}

              {(!!hideUnder === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueDropDown4}
                    backColor={'white'}
                    color={'#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    isArrow={true}
                    onPress={() => props.onEventClick(3, title2, 'Under')}
                  />
                  <View style={pickerView}>
                    <PickerController
                      itemsData={getArrayList(data2)}
                      type={'filter1'}
                      onOpenPicker={props.onOpenPicker}
                      onValueChange={(value) =>
                        props.onValueChange(value, 'Under', title2)
                      }
                      onFilterUpdate={() =>
                        props.onFilterUpdate(title2, 'Under')
                      }
                    />
                  </View>
                </View>
              )) ||
                emptyData()}
            </View>

            <View>
              {(!!hideOver === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueSelectedValue2}
                    backColor={
                      (boolSelected2 && Constant.appTextColor) || 'white'
                    }
                    color={(boolSelected2 && 'white') || '#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    onPress={() => props.onEventClick(3, title, 'Under')}
                  />
                </View>
              )) ||
                emptyData()}

              {(!!hideUnder === false && (
                <View style={mainView}>
                  <ButtonSelection
                    title={valueSelectedValue4}
                    backColor={
                      (boolSelected4 && Constant.appTextColor) || 'white'
                    }
                    color={(boolSelected4 && 'white') || '#000000'}
                    shadow={Constant.shadow}
                    otherStyle={buttonStyle}
                    otherTextStyle={buttonText}
                    onPress={() => props.onEventClick(3, title2, 'Under')}
                  />
                </View>
              )) ||
                emptyData()}
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

function emptyData() {
  return <View style={[styles.mainView]} />;
}

const getArrayList = (data) => {
  let objArray = [];
  for (let i = 0; i < data.length; i++) {
    if (data[i].value) {
      objArray.push({value: `${data[i].value}`, label: `${data[i].value}`});
    }
  }
  return objArray;
};

const styles = StyleSheet.create({
  titleFont: {
    fontSize: 16,
    fontFamily: 'System',
    fontWeight: 'bold',
    color: Constant.appTextColorDark,
    textAlign: 'center',
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 15,
    fontFamily: 'System',
    fontWeight: '600',
    paddingHorizontal: 10,
  },
  buttonStyle: {
    height: 30,
    borderRadius: 19,
  },
  mainView: {
    minHeight: 40,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  pickerView: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
});

export {OverUnderMixSection};
