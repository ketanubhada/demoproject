import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Constant from '../../../../helper/constant';
import {ButtonSelection} from '../../../commonComponent/buttonSelection';

const OnlyTextSubSection = (props) => {
  const {data, title} = props;
  const {titleFont, buttonText, buttonStyle} = styles;

  return (
    <View>
      <View style={{flex: 1}} />
      <Text style={[titleFont]}>{title}</Text>

      <View
        style={{
          minHeight: 40,
          justifyContent: 'center',
          paddingHorizontal: 10,
        }}>
        <ButtonSelection
          title={data.Away}
          backColor={
            (data.isSelected &&
              data.isSelected.includes('Away') &&
              Constant.appTextColor) ||
            'white'
          }
          color={
            (data.isSelected && data.isSelected.includes('Away') && 'white') ||
            '#000000'
          }
          shadow={Constant.shadow}
          otherStyle={buttonStyle}
          otherTextStyle={buttonText}
          onPress={() => props.onEventClick(1, title, 'Away')}
        />
      </View>

      <View
        style={{
          minHeight: 40,
          justifyContent: 'center',
          paddingHorizontal: 10,
        }}>
        <ButtonSelection
          title={data.Home}
          backColor={
            (data.isSelected &&
              data.isSelected.includes('Home') &&
              Constant.appTextColor) ||
            'white'
          }
          color={
            (data.isSelected && data.isSelected.includes('Home') && 'white') ||
            '#000000'
          }
          shadow={Constant.shadow}
          otherStyle={buttonStyle}
          otherTextStyle={buttonText}
          onPress={() => props.onEventClick(1, title, 'Home')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  titleFont: {
    fontSize: 16,
    fontFamily: 'System',
    fontWeight: 'bold',
    color: Constant.appTextColorDark,
    textAlign: 'center',
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 15,
    fontFamily: 'System',
    fontWeight: '600',
    paddingHorizontal: 10,
  },
  buttonStyle: {
    height: 30,
    borderRadius: 19,
  },
});

export {OnlyTextSubSection};
