import React from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import Button from '../screens/commonComponent/button';
import SafeArea from 'react-native-safe-area';
import {cloneDeep} from 'lodash';
import {setSafeAreaIntent} from '../actions/userActions';
import Constant from './../helper/constant';

class InitializeApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.setSafeArea();
  }

  setSafeArea = () => {
    SafeArea.getSafeAreaInsetsForRootView().then((result) => {
      let temp = {
        top:
          result.safeAreaInsets.top > 0
            ? result.safeAreaInsets.top - 20
            : result.safeAreaInsets.top,
        bottom: result.safeAreaInsets.bottom,
        left: result.safeAreaInsets.left,
        right: result.safeAreaInsets.right,
      };
      let obj = cloneDeep(temp);
      this.props.setSafeAreaIntent(obj);
      SafeArea.removeEventListener(
        'safeAreaInsetsForRootViewDidChange',
        this.onSafeAreaInsetsForRootViewChange,
      );
    });
  };

  onSafeAreaInsetsForRootViewChange = () => {};

  onButtonStart = () => {
    this.props.navigation.navigate('rootTabNavigation', {});
  };

  render() {
    const {container} = styles;

    return (
      <View style={[container]}>
        <Button
          title={'Start'}
          onPress={this.onButtonStart}
          backColor="#FFFFFF"
          color={Constant.appTextColor}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {
  setSafeAreaIntent,
})(InitializeApp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.appBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
