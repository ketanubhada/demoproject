import React, {Component, useContext, useState} from 'react';
import {View, StyleSheet, TextInput, TouchableOpacity, Text, FlatList} from 'react-native';
import {connect} from 'react-redux';
import Constant from '../../../helper/constant';
import {saveMyData} from '../../../actions/userActions';


function StatisticTab(props) {

    const [arrData, setArrData] = useState(props.getArrayObject);

    const [textValue, setTextValue] = useState("")

    const onButtonSave = () => {
        //Append new object in array
        setArrData([...arrData, textValue]);

        //Save data in reduct
        props.saveMyData(arrData);

        //clear text input
        setTextValue("")
    }

    // Header for search and button click
    const renderHeader = () => {
        return(
            <View style={{width: '100%', marginTop: 50, flexDirection: 'row', marginBottom: 15}}>
                <TextInput
                    value={textValue}
                    placeholder={"Input name"}
                    style={{flex: 1, marginLeft: 20, backgroundColor: 'pink', padding: 15}}
                    returnKeyType={'next'}
                    onChangeText={(text) => setTextValue(text)}/>

                <TouchableOpacity
                    style={{
                        width: 50,
                        height: '100%',
                        marginRight: 20,
                        backgroundColor: 'black',
                        alignItems: 'center',
                        marginLeft: 10
                    }}
                    onPress={onButtonSave}
                >
                    <Text style={{color: 'white', alignSelf: 'center', marginTop: 15, fontSize: 15}}>{"Save"}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    //Footer render method
    const renderRow = ({item, index}) => {
        return(
            <View style={{backgroundColor: 'pink', marginBottom: 10, alignItems: 'center', justifyContent: 'center', marginHorizontal: 20}}>
                <Text style={{color: 'white', alignSelf: 'center',fontSize: 15, marginVertical: 10}}>{item}</Text>
            </View>
        )
    }

    return (
      <View style={{flex:1,backgroundColor: 'red'}}>
          {renderHeader()}

          <FlatList showsVerticalScrollIndicator={true}
                    keyboardShouldPersistTaps='always'
                     removeClippedSubviews={false}
                    data={arrData}
                    renderItem={renderRow}/>

      </View>
  )
}

const mapStateToProps = state => {
  return {
    safeAreaInsetsDefault: state.user.safeAreaInsetsDefault,
      getArrayObject: state.user.arrDataList,
  };
};

export default connect(mapStateToProps, {
    saveMyData
})(StatisticTab);
