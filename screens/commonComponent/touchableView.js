import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';

export default class TochableView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backColor: props.backColor,
      opacity: 1,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.pressInColor !== nextProps.pressInColor ||
      this.props.backColor !== nextProps.backColor
    ) {
      this.setState({
        backColor: nextProps.backColor,
        opacity: 1,
      });
    }
  }

  onRowPress = () => {
    if (this.props.isHighlightDisable !== undefined) {
      this.props.onPress();
    } else {
      this.setState(
        {
          backColor: this.props.pressInColor,
          opacity: 0.5,
        },
        () => {
          setTimeout(() => {
            this.props.onPress();
          }, 0);
          setTimeout(() => {
            this.setState({
              backColor: this.props.backColor,
              opacity: 1,
            });
          }, 100);
        },
      );
    }
  };

  render() {
    return (
      <View
        style={[
          {backgroundColor: this.state.backColor, opacity: this.state.opacity},
          (this.props.style && this.props.style) || null,
        ]}>
        {this.props.children}
        {!!(this.props.touchbleOff === undefined) && (
          <TouchableOpacity
            style={{
              left: 0,
              right: this.props.rightSpace || 0,
              top: 0,
              bottom: 0,
              position: 'absolute',
            }}
            disabled={this.props.disabled || false}
            onPress={this.onRowPress}
          />
        )}
      </View>
    );
  }
}
