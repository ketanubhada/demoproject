import React, {Component} from 'react';
import {Text, StyleSheet, Image, View} from 'react-native';
import TochableView from './touchableView';

const ButtonSelection = (props) => {
  const {title} = props;
  const {btnStyle, imageArrow} = styles;

  return (
    <View style={{zIndex: 0}}>
      <TochableView
        onPress={props.onPress}
        disabled={props.isDisable || false}
        style={[
          btnStyle,
          {flexDirection: 'row', backgroundColor: props.backColor},
          props.otherStyle ? props.otherStyle : {},
          props.shadow && props.shadow,
        ]}>
        <Text
          style={[
            styles.btnFont,
            {color: props.color},
            props.otherTextStyle ? props.otherTextStyle : {},
          ]}>
          {title}
        </Text>
        {!!props.isArrow && (
          <Image
            resizeMode="contain"
            style={imageArrow}
            source={{uri: 'bottom_arrow'}}
          />
        )}
      </TochableView>
    </View>
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
  },
  btnFont: {
    fontSize: 16,
    fontFamily: 'System',
    fontWeight: '400',
  },
  imageArrow: {
    height: 16,
    width: 9,
    marginRight: 12,
    marginTop: 1.5,
  },
});

export {ButtonSelection};
