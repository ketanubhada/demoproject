import React, {Component} from 'react';
import {ActivityIndicator, View, StyleSheet} from 'react-native';
import Constant from '../../helper/constant';

export default class InitialScreen extends React.Component {
  render() {
    let topNavigation = 20;
    return (
      <View style={styles.container}>
        {/*<Image source={{uri:Constant.isIOS && 'splash_icon' || 'splash_icon2'}}*/}
        {/*       resizeMode={Constant.isIOS && 'center' || 'contain'}*/}
        {/*       style={[styles.icon,{ marginTop: Constant.isANDROID && topNavigation || 0}]}/>*/}
        <View style={styles.indicator}>
          <ActivityIndicator animating={true} size="small" color={'#FFF'} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: 100,
    width: 100,
  },
  indicator: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: Constant.screenHeight * 0.92,
  },
});
