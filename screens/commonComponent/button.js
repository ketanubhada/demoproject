import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Constant from '../../helper/constant';

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        disabled={this.props.isDisable || false}
        style={[
          styles.btnLogin,
          {backgroundColor: this.props.backColor},
          this.props.otherStyle ? this.props.otherStyle : {},
        ]}>
        {(this.props.isActivityIndicator && (
          <ActivityIndicator color={'#FFF'} />
        )) || (
          <View style={{flexDirection: 'row'}}>
            <Text
              style={[
                styles.btnFont,
                {color: this.props.color},
                this.props.otherTextStyle ? this.props.otherTextStyle : {},
              ]}>
              {this.props.title}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  btnLogin: {
    height: 60,
    marginTop: 30,
    marginLeft: 30,
    marginRight: 30,
    alignSelf: 'center',
    width: Constant.screenWidth - 48,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 35,
    maxWidth: (Constant.isIpad && 400) || 450,
  },
  btnFont: {
    fontSize: 16,
    fontFamily: 'System',
    fontWeight: 'bold',
    color: 'white',
  },
});
