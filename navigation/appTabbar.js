import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Constant from '../helper/constant';
import {connect} from 'react-redux';
import {EventRegister} from 'react-native-event-listeners';

//Tab navigation
const Tab = createBottomTabNavigator();
let InitRootName = 'Today';
let tabLeftSpace = Constant.screenWidth / 2;

//Tab
import TodayStackNavigator from '../screens/today/todayPage';
import StatisticTabNavigator from '../screens/tabs/statistic/statisticTab';
import TabbarIcon from './subComponent/tabbarIcon';

const RootTabs2 = () => {
  return (
    <Tab.Navigator
      initialRouteName={InitRootName}
      tabBarPosition={'bottom'}
      tabBarOptions={{
        showLabel: false,
        style: {
          backgroundColor: 'transparent',
          borderTopWidth: 0,
        },
        tabStyle: {},
      }}>
      <Tab.Screen
        name="Today"
        component={TodayStackNavigator}
        options={{
          tabBarIcon: ({tintColor, focused}) => {
            return (
              <TabbarIcon
                tintColor={tintColor}
                tabbar={'Today'}
                focused={focused}
                marginLeft={tabLeftSpace}
              />
            );
          },
          tabBarOnPress: ({navigation, defaultHandler}) => {
            EventRegister.emit('tabChangeListner', 'today');
            return defaultHandler();
          },
        }}
      />
      <Tab.Screen
        name="Statistic"
        component={StatisticTabNavigator}
        options={{
          tabBarIcon: ({tintColor, focused}) => {
            return (
              <TabbarIcon
                tintColor={tintColor}
                tabbar={'Statistic'}
                focused={focused}
                marginLeft={tabLeftSpace / 2}
              />
            );
          },
          tabBarOnPress: ({navigation, defaultHandler}) => {
            EventRegister.emit('tabChangeListner', 'statistic');
            return defaultHandler();
          },
        }}
      />
    </Tab.Navigator>
  );
};

class RootTabNavigation extends React.Component {
  // static router = RootTabs.router;

  constructor(props) {
    super(props);

    this.state = {
      isTouchEnable: false,
    };
  }

  render() {
    return (
      <View style={[styles.container, {backgroundColor: '#FFFFFF'}]}>
        {/*<StatusBar hidden={false} barStyle={appColor.statusBarStyle}/>*/}
        <RootTabs2
          ref={(c) => (this._rootTabView = c)}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.backColor,
    height: Constant.screenHeight,
    marginTop: 0,
    overflow: 'hidden',
  },
});

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {})(RootTabNavigation);
