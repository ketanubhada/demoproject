import React from 'react';
import {Image, View, Text} from 'react-native';
import Constant from '../../helper/constant';
import {connect} from 'react-redux';

class TabbarIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View
        style={{
          zIndex: 0,
          width: Constant.screenWidth / 2,
          height: 50,
          justifyContent: 'center',
          alignSelf: 'center',
          backgroundColor: '#FFF',
        }}>
        <Text
          style={{
            fontFamily: 'System',
            fontWeight: '600',
            color:
              (!this.props.focused && '#00000') || Constant.appTextColorDark,
            textAlign: 'center',
          }}>
          {this.props.tabbar}
        </Text>
        <View
          style={{
            top: 0,
            left: 0,
            right: 0,
            height: 1,
            backgroundColor: Constant.appTextColorLight,
            position: 'absolute',
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {})(TabbarIcon);
