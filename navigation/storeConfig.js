import React from 'react';
import {View, AsyncStorage} from 'react-native';
import {Provider, connect} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

import {NavigationContainer} from '@react-navigation/native';
import {enableScreens} from 'react-native-screens';
import storage from 'redux-persist/lib/storage';
import {persistReducer, persistStore} from 'redux-persist';
import getStoredStateMigrateV4 from 'redux-persist/lib/integration/getStoredStateMigrateV4';
import AppReducer from '../reducers/index';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Screen} from './screens';

import InitialSplashView from '../screens/commonComponent/initialSplashScreen';

const persistConfig = {
  key: 'root',
  storage,
  getStoredState: getStoredStateMigrateV4({
    blacklist: ['navigation'],
    storage: AsyncStorage,
  }),
  timeout: 0,
};
const persistedReducer = persistReducer(persistConfig, AppReducer);
let store = createStore(persistedReducer, applyMiddleware(thunk));
let persistor = persistStore(store);

enableScreens();

console.disableYellowBox = true;

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  UNSAFE_componentWillMount() {}

  componentDidMount() {}

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<InitialSplashView />} persistor={persistor}>
          <View style={{flex: 1, backgroundColor: 'red'}}>
            <NavigationContainer>
              <Screen />
            </NavigationContainer>
          </View>
        </PersistGate>
      </Provider>
    );
  }
}
