import React from 'react';

//Welcome Flow
import initialPage from '../screens/initialComponent';

//Tab bar
import tabbarView from './appTabbar';

import {createNativeStackNavigator} from 'react-native-screens/native-stack';

const Stack = createNativeStackNavigator();

export function Screen() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name="initialPage"
        options={{stackAnimation: 'none'}}
        component={initialPage}
      />
      <Stack.Screen
        name="rootTabNavigation"
        options={{gestureEnabled: false}}
        component={tabbarView}
      />
    </Stack.Navigator>
  );
}
