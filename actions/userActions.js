import React, {Component} from 'react';
import {SET_SAFE_AREA_INTENT, SET_USER_SAVE_DATA} from './types';
import AppConstant from '../helper/constant';

export const setSafeAreaIntent = (data) => {
  return (dispatch, getState) => {
    if (AppConstant.isIOS) {
      return dispatch({
        type: SET_SAFE_AREA_INTENT,
        payload: data,
      });
    }
  };
};

export const saveMyData = (data) => {
  return (dispatch, getState) => {
    if (AppConstant.isIOS) {
      return dispatch({
        type: SET_USER_SAVE_DATA,
        payload: data,
      });
    }
  };
};
