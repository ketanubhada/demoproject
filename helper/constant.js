import {Dimensions, Platform} from 'react-native';

module.exports = {
  screen: Dimensions.get('window'),
  screenHeight:
    (Platform.OS === 'ios' && Dimensions.get('window').height) ||
    Dimensions.get('window').height - 24,
  screenWidth: Dimensions.get('window').width,
  fullScreenHeight: Dimensions.get('window').height,

  isIphoneX: Platform.OS === 'ios' && Dimensions.get('window').height === 812,

  isIOS: Platform.OS === 'ios',
  isiPAD:
    Dimensions.get('window').height / Dimensions.get('window').width < 1.6,
  isIpad:
    Dimensions.get('window').width > 400 &&
    Dimensions.get('window').height / Dimensions.get('window').width < 1.6,
  // isiPad: Device.isIpad(),
  isANDROID: Platform.OS === 'android',

  shadow: {
    shadowColor: '#000000',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.06,
    shadowRadius: 4,
    elevation: 3,
  },
  appBackgroundColor: '#015c92',
  appTextColor: '#88CDF2',
  appTextColorDark: '#2D82B5',
  appTextColorLight: '#BCE6EF',
  transparent: 'transparent',
};
