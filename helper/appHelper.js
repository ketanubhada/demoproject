import React, {Component} from 'react';
import {find} from 'lodash';

export function checkBadgeCount(data) {
  let mainArray = Object.keys(data);

  let countBadge = 0;

  mainArray.forEach((currentItem, index) => {
    if (currentItem !== 'mainList') {
      let subArray = Object.keys(data[currentItem].data);

      subArray.forEach((currentItemSub, index) => {
        let arr_Obj = Object.keys(data[currentItem].data[currentItemSub]);
        let dataNew = data[currentItem].data[currentItemSub];

        arr_Obj.forEach((currentItem, index) => {
          if (dataNew[currentItem].isSelected) {
            countBadge = countBadge + dataNew[currentItem].isSelected.length;
          } else {
            let obj = find(dataNew[currentItem], 'isSelected');
            if (obj) {
              countBadge = countBadge + obj.isSelected.length;
            }
          }
        });
      });
    }
  });

  return countBadge;
}
