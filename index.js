/**
 * @format
 */
import {AppRegistry} from 'react-native';
import App from './navigation/storeConfig';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
