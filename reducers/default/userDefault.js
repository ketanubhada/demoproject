import Constant from '../../helper/constant';

export const userDefault = {
  userName: '',
  safeAreaInsetsDefault: {top: 0, bottom: 0, left: 0, right: 0},
  safeAreaInsetsData: {top: 0, bottom: 0, left: 0, right: 0},
  arrDataList: []
};
