import {combineReducers} from 'redux';
// import  AppNavigator  from '../navigation/index';
import UserReducer from './userReducer';
import {appDefaultReducer} from './defaultReducer';

import {Screen} from '../navigation/screens';

const appReducer = combineReducers({
  user: UserReducer,
});

export default function rootReducer(state, action) {
  let finalState = appReducer(state, action);
  return finalState;
}
