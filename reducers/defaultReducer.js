import {userDefault} from './default/userDefault';

export const appDefaultReducer = {
  user: userDefault,
};
