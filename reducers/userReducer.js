import {START_LOADING, SET_SAFE_AREA_INTENT, SET_USER_SAVE_DATA} from '../actions/types';
import _ from 'lodash';
import {appDefaultReducer} from './defaultReducer';

const INITIAL_STATE = appDefaultReducer.user;

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case START_LOADING: {
      return {
        ...state,
        isLoading: action.payload,
      };
    }
    case SET_SAFE_AREA_INTENT: {
      return {
        ...state,
        safeAreaInsetsDefault: action.payload,
        safeAreaInsetsData: action.payload,
      };
    }
    case SET_USER_SAVE_DATA: {
      return {
        ...state,
        arrDataList: action.payload,
      };
    }
    default:
      return state;
  }
};
